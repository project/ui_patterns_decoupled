<?php

namespace Drupal\ui_patterns_decoupled\Normalizer;

use Drupal\layout_builder\Normalizer\SectionDataNormalizer;
use Drupal\layout_builder\Plugin\DataType\SectionData;
use Drupal\ui_patterns\UiPatterns;
use Drupal\ui_patterns_settings\UiPatternsSettings;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class UiPatternsNormalizer extends SectionDataNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = SectionData::class;

  public static function prepareSection($section_ary, $entity = NULL) {
    if (isset($section_ary['layout_settings']['pattern']) &&
      isset($section_ary['layout_id'])) {

      $pattern_id = str_replace('pattern_', '', $section_ary['layout_id']);
      $pattern = UiPatterns::getManager()->getDefinition($pattern_id);
      $component = $pattern->getAdditional()['component'] ?? NULL;
      $settings = $section_ary['layout_settings']['pattern']['settings'] ?? [];
      $variant = $section_ary['layout_settings']['pattern']['variant'] ?? NULL;
      $section_ary['layout_settings']['pattern']['id'] = $pattern_id;
      $section_ary['layout_settings']['pattern']['component'] = $component;
      $section_ary['layout_settings']['pattern']['settings'] = UiPatternsSettings::preprocess($pattern_id, $settings, $variant, FALSE, $entity);
      if (isset($section_ary['components'])) {
        foreach ($section_ary['components'] as &$component) {
          if (isset($component['configuration']['pattern']) && $component['configuration']['id']) {
            $pattern_id = str_replace('pattern_block:', '', $component['configuration']['id']);
            $pattern = UiPatterns::getManager()->getDefinition($pattern_id);
            $pattern_component = $pattern->getAdditional()['component'] ?? NULL;
            $settings = $component['configuration']['pattern']['settings'] ?? [];
            $variant = $component['configuration']['pattern']['variant'] ?? NULL;
            $component['configuration']['pattern']['id'] = $pattern_id;
            $component['configuration']['pattern']['component'] = $pattern_component;
            $component['configuration']['pattern']['settings'] = UiPatternsSettings::preprocess($pattern_id, $settings, $variant, FALSE, $entity);
          }
        }
      }

    }
    return $section_ary;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {

    $entity_type_manager = \Drupal::entityTypeManager();
    $resource_object = $context['resource_object'];
    /** @var \Drupal\jsonapi\JsonApiResource\ResourceObject $resource_object */
    $revision_id = str_replace('id:', '', $resource_object->getVersionIdentifier());
    $entity_type_id = $resource_object->getResourceType()->getEntityTypeId();
    if ($revision_id !== NULL) {
      $entity = $entity_type_manager->getStorage($entity_type_id)->loadRevision($revision_id);
    }
    else {
      $uuid = $resource_object->getId();
      $entity = $entity_type_manager->getStorage($entity_type_id)->loadByProperties(['uuid' => $uuid]);
    }

    $section_ary = parent::normalize($object, $format, $context);
    $section_ary = UiPatternsNormalizer::prepareSection($section_ary, $entity);
    return $section_ary;
  }

}

<?php

namespace Drupal\ui_patterns_decoupled\Plugin\jsonapi\FieldEnhancer;

use Drupal\jsonapi_extras\Plugin\ResourceFieldEnhancerBase;
use Drupal\ui_patterns_decoupled\Normalizer\UiPatternsNormalizer;
use Shaper\Util\Context;

/**
 * Perform additional manipulations to date fields.
 *
 * @ResourceFieldEnhancer(
 *   id = "ui_patterns_decoupled_section_data",
 *   label = @Translation("UI Patterns Section Data Enhander"),
 *   description = @Translation("Extracts UI Patterns Configuration.")
 * )
 */
class SchemaSectionDataEnhancer extends ResourceFieldEnhancerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'path' => 'value',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doUndoTransform($data, Context $context) {
    $output = $data;

    $sections = $output['layout_builder']['sections'] ?? [];
    foreach ($sections as $i => $section) {
      /** @var \Drupal\jsonapi\Serializer\Serializer $serializer */
      $serializer = \Drupal::service('serializer');
      $normalized = $serializer->normalize($section->toArray());
      $output['layout_builder']['sections'][$i] = UiPatternsNormalizer::prepareSection($normalized);
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransform($data, Context $context) {
    $input = $data;
    return $input;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputJsonSchema() {
    return [
      'oneOf' => [
        ['type' => 'object'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $resource_field_info) {
    $settings = empty($resource_field_info['enhancer']['settings'])
      ? $this->getConfiguration()
      : $resource_field_info['enhancer']['settings'];

    return [
      'path' => [
        '#type' => 'textfield',
        '#title' => $this->t('Path'),
        '#description' => $this->t('A dot separated path to extract the sub-property.'),
        '#default_value' => $settings['path'],
      ],
    ];
  }

}

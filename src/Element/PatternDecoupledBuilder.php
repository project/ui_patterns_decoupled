<?php

namespace Drupal\ui_patterns_decoupled\Element;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Template\Attribute;
use Drupal\ui_patterns\UiPatterns;
use Drupal\ui_patterns_settings\UiPatternsSettings;

/**
 * Renders a pattern element.
 */
class PatternDecoupledBuilder implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['processLayoutBuilderComponents', 'processPreviewLayoutBuilderComponents'];
  }

  private static function loadJsonApiEntity($entity_type, $entity_id) {
    $sub_request = Request::create('/jsonapi/' . $entity_type . '/' . $entity_id, 'GET');
    $http_kernel = \Drupal::service('http_kernel.basic');
    $subResponse = $http_kernel->handle($sub_request, HttpKernelInterface::SUB_REQUEST);
    $html = $subResponse->getContent();
  }

  /**
   * Process layout builder regions.
   *
   * Layout builder adds foreach region an renderable array
   * after the pattern is built. So reassign the region to fields.
   *
   * @param array $element
   *   Render array.
   *
   * @return array
   *   Render array.
   */
  public static function processLayoutBuilderComponents(array $element, $preview = FALSE) {
    $definiton = UiPatterns::getPatternDefinition($element['#id']);
    $component = $definiton->getAdditional()['component'] ?? NULL;
    $library = $definiton->getAdditional()['library'] ?? 'vue';
    if ($component != NULL && $definiton !== NULL) {
      $element['#theme'] = 'ui_patterns_decoupled_' . $library;
      $element['#fields'] = [];
      $element['#component'] = $component;
      $fields = $definiton->getFields();
      foreach ($fields as $key => $field) {
        if (isset($element[$key])) {
          $element['#fields'][$key] = $element[$key];
        } else if (isset($element['#'. $key])) {
          $element['#fields'][$key] = $element['#'. $key];
        } else if ($preview === TRUE) {
          $element['#fields'][$key] = $fields[$key]->getPreview();
        }
      }
      if (\Drupal::moduleHandler()->moduleExists('ui_patterns_settings')) {
        $element['#settings'] = [];
        $definition = UiPatterns::getPatternDefinition($element['#id']);
        $settings = UiPatternsSettings::getPatternDefinitionSettings($definition);
        $setting_attributes = new Attribute();
        foreach ($settings as $key => $setting) {
          if (isset($element['#' . $key])) {
            $element['#settings']['#' . $key] = $element['#' . $key];
            $setting_attributes->setAttribute($key, $element['#' . $key]);
          }
        }
        if (isset($element['#variant']) && !empty($element['#variant'])) {
          $setting_attributes->setAttribute('variant', $element['#variant']);
        }
        $element['#setting_attributes'] = $setting_attributes;
      }
    }
    return $element;
  }

  /**
   * Process layout builder regions.
   *
   * Layout builder adds foreach region an renderable array
   * after the pattern is built. So reassign the region to fields.
   *
   * @param array $element
   *   Render array.
   *
   * @return array
   *   Render array.
   */
  public static function processPreviewLayoutBuilderComponents(array $element) {
    return PatternDecoupledBuilder::processLayoutBuilderComponents($element, TRUE);
  }

}

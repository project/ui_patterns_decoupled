<?php

namespace Drupal\ui_patterns_decoupled;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\ui_patterns_decoupled\Normalizer\UiPatternsNormalizer;
use Symfony\Component\DependencyInjection\ChildDefinition;

/**
 * Sets the layout_builder.get_block_dependency_subscriber service definition.
 *
 * This service is dependent on the block_content module so it must be provided
 * dynamically.
 *
 * @internal
 *   Service providers are internal.
 *
 * @see \Drupal\layout_builder\EventSubscriber\SetInlineBlockDependency
 */
class UiPatternsDecoupledServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['serialization'])) {
      $definition = (new ChildDefinition('serializer.normalizer.config_entity'))
        ->setClass(UiPatternsNormalizer::class)
        ->addTag('normalizer', ['priority' => 6]);
      $container->setDefinition('ui_patterns_decoupled.normalizer.layout_entity_display', $definition);
      $section_definition = (new ChildDefinition('serializer.normalizer.typed_data'))
        ->setClass(UiPatternsNormalizer::class)
        ->addTag('normalizer', ['priority' => 6]);
      $container->setDefinition('ui_patterns_decoupled.normalizer.section_data', $section_definition);
    }
  }

}

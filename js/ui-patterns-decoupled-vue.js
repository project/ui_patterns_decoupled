Drupal.behaviors.UIPatternsDecoupledVue = {
  components: {},
  attach() {
    const wrappers = window.document.querySelectorAll('.ui-patterns-decoupled-vue');
    const components = drupalSettings.UIPatternsDecoupled.vue.components ?? [];
    const provides = drupalSettings.UIPatternsDecoupled.vue.provides ?? [];
    const body = window.document.getElementsByTagName('body')[0];
    wrappers.forEach((wrapper) => {

      if (wrapper) {
        if (!wrapper.classList.contains('vue--processed') && jQuery(wrapper).parents('.ui-patterns-decoupled-vue').length === 0) {
          try {
            wrapper.classList.add('vue--processed');
            const app = Vue.createApp({
              mounted:() => {
                Drupal.attachBehaviors(wrapper);
                body.setAttribute('style', 'height:' + body.offsetHeight + 'px');
              }
            });
            Object.keys(provides).forEach((providesName) => {
              app.provide(providesName, provides[providesName]);
            });
            Object.keys(components).forEach((componentName) => {
              app.component(componentName, components[componentName]);
            });
            app.mount(wrapper)
          } catch (e) {
            console.error('Unable to mount VUE component.');
            console.error('Available components:')
            console.log(components)
            console.error(e);
          }
        }
      }
    });
  },
}

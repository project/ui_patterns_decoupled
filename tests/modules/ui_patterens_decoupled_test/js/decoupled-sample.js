const section = {
  setup() {
  return { }
},
  template: `
    <div>SECTION
      <slot name="column_1"></slot>
    </div>`
}

const text = {
  setup() {
    return { }
  },
  template: `
    <div>TEXT
        <div> MY Component
        <slot name="text"></slot>
      </div>
    </div>`
}

console.log('ADD Components')
drupalSettings.UIPatternsDecoupled.vue.components['ws-section'] = section;
drupalSettings.UIPatternsDecoupled.vue.components['ws-text'] = text;
